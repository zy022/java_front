import request from '@/utils/request'

//常量
const apiName = '/admin/system/sysMenu/'

export default
{
    //菜单列表（树视图
    getAllMenu(){
        return request({
            // url: '/admin/system/sysRole/' + page+"/"+limit,//接口路径
            url: `${apiName}/GetAllMenu`,//接口路径
            method: 'get',//提交方式
        })
    },
    //添加菜单
    addMenu(sysMenu){
        return request({
            url: `${apiName}/AddMenu`,//接口路径
            method: 'post',//提交方式
            data:sysMenu
        })
    },
    //修改菜单
    getMenuById(id){
        return request({
            url: `${apiName}/GetMenuById`+`${id}`,//接口路径
            method: 'get',//提交方式
        })
    },
    //修改菜单
    updateMenu(sysMenu){
        return request({
            url: `${apiName}/UpdateMenu`,//接口路径
            method: 'post',//提交方式
            data:sysMenu
        })
    },
    //删除菜单
    removeMenu(id){
        return request({
            url: `${apiName}/RemoveMenu/`+`${id}`,//接口路径
            method: 'delete',//提交方式
            params:id
        })
    }
}