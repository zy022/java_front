import request from '@/utils/request'

//常量
const apiName = '/admin/system/sysRoleMenu/'

export default
{
    /*
    查看某个角色的权限列表
    */
    getRoleAssignAuthorityById(roleId) {
        return request({
        url: `${apiName}/GetRoleAssignAuthorityById/${roleId}`,
        method: 'get'
        })
    }, 
    /*
    给某个角色授权
    */
    assignAuthorityToRole(assginMenuVo) {
        return request({
        url: `${apiName}/AssignAuthorityToRole`,
        method: "post",
        data: assginMenuVo
        })
    }
}