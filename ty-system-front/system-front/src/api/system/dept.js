import request from '@/utils/request'

//常量
const apiName = '/admin/system/sysDept/'

export default {
  //列表
  getPageList(page, limit, searchObj) {
    return request({
      // url: '/admin/system/sysRole/' + page+"/"+limit,//接口路径
      url: `${apiName}/${page}/${limit}`, //接口路径
      method: 'get', //提交方式
      params: searchObj //提交的其他参数
    })
  },
  //删除部门管理用户
  removeUserById(params) {
    return request({
      // url: '/admin/system/sysRole/' + page+"/"+limit,//接口路径
      url: `${apiName}/deleteDept`, //接口路径
      method: 'get', //提交方式
      params //提交的其他参数
    })
  },
  //新增
  addDeptUser(data) {
    return request({
      // url: '/admin/system/sysRole/' + page+"/"+limit,//接口路径
      url: `${apiName}/addDept`, //接口路径
      method: 'post', //提交方式
      data //提交的其他参数
    })
  }

}
