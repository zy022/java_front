import request from '@/utils/request'

//常量
const apiName = '/admin/system/sysRole/'

export default {
    //列表
    getPageList(page,limit,searchObj){
        return request({
           // url: '/admin/system/sysRole/' + page+"/"+limit,//接口路径
            url: `${apiName}/${page}/${limit}`,//接口路径
            method: 'get',//提交方式
            params :searchObj //提交的其他参数
          })
    },
    //根据id删除
    removeById(id){
        return request({
            url: `${apiName}/remove/${id}`,//接口路径
            method: 'delete',//提交方式
          })
    },
    //添加角色
    addRole(role){
        return request({
            url: `${apiName}/save`,//接口路径
            method: 'post',//提交方式
            data :role //提交的其他参数--json格式
          })
    },
    //根据id查询
    findRoleById(id){
        return request({
            url: `${apiName}/findRoleById/${id}`,//接口路径
            method: 'get',//提交方式
          })
    },
    //修改
    updateRole(role){
        return request({
            url: `${apiName}/update`,//接口路径
            method: 'post',//提交方式
            data :role //提交的其他参数--json格式
          })
    },
    //批量删除
    batchRemove(idList){
        return request({
            url: `${apiName}/batchRemove`,//接口路径
            method: 'delete',//提交方式
            data :idList //提交的其他参数--json格式
          })
    },
    //根据用户获取角色数据
    getRolesByUserId(id){
        return request({
            url: `${apiName}/GetRolesByUserId/${id}`,//接口路径
            method: 'get',//提交方式
          })
    },
    //根据用户分配角色
    assignRolesToUser(assginRoleVo){
        return request({
            url: `${apiName}/AssignRolesToUser`,//接口路径
            method: 'post',//提交方式
            data :assginRoleVo //提交的其他参数--json格式
          })
    }
}