import request from '@/utils/request'

//常量
const apiName = '/admin/system/sysUser/'

export default {
    //列表
    getUserPageList(page,limit,searchObj){
        return request({
           // url: '/admin/system/sysRole/' + page+"/"+limit,//接口路径
            url: `${apiName}/${page}/${limit}`,//接口路径
            method: 'get',//提交方式
            params :searchObj //提交的其他参数
        })
    },
    //添加接口
    addUser(user){
        return request({
            url: `${apiName}/addUser`,//接口路径
            method: 'post',//提交方式
            data :user //提交的其他参数--json格式
        })
    },
    //根据id获取用户信息
    getUserById(id){
        return request({
            url: `${apiName}/getUser/${id}`,//接口路径
            method: 'get',//提交方式
        })
    },
    //修改用户信息
    updateUser(user){
        return request({
            url: `${apiName}/update`,//接口路径
            method: 'post',//提交方式
            data :user //提交的其他参数--json格式
        })
    },
    //删除用户
    removeUserById(id){
        return request({
            url: `${apiName}/removeUser/${id}`,//接口路径
            method: 'delete',//提交方式
        })
    },
    //批量删除用户
    batchRemoveUser(idList){
        return request({
            url: `${apiName}/batchRemoveUser`,//接口路径
            method: 'delete',//提交方式
            data :idList //提交的其他参数--json格式
        })
    },
    //更改用户状态
    userStatusChange(id, status){
         return request({
            url: `${apiName}/userStatusChange/${id}/${status}`,//接口路径
            method: 'get',//提交方式
        })
    }
}